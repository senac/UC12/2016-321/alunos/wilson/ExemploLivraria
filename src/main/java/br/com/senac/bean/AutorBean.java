/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.bean;

import br.com.senac.banco.AutorDao;
import br.com.senac.livraria.entity.Autor;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

/**
 *
 * @author Administrador
 */
@Named(value = "autorBean")
@ViewScoped
public class AutorBean extends Bean {
    
    private Autor autor;
    
    private AutorDao dao;
    
    public AutorBean() {
        
    }
    
    @PostConstruct
    public void init() {
        this.autor = new Autor();
        this.dao = new AutorDao();
    }
    
    public String getCodigo() {
        return this.autor.getId() == 0 ? "" : String.valueOf(this.autor.getId());
    }
    
    public void novo() {
        this.autor = new Autor();
    }
    
    public void salvar() {
        
        try {
            if (this.autor.getId() == 0) {
                dao.save(autor);
                addMessageInfo("Salvo com sucesso!");
            } else {
                dao.update(autor);
                addMessageInfo("Alterado com sucesso!");
            }
            
        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }
        
    }
    
    public void excluir(Autor autor) {
        try {
            
            dao.delete(autor.getId());
            addMessageInfo("Removido com sucesso!");
            
        } catch (Exception ex) {
            addMessagemErro(ex.getMessage());
        }
        
    }
    
    public Autor getAutor() {
        return autor;
    }
    
    public void setAutor(Autor autor) {
        this.autor = autor;
    }
    
    public List<Autor> getLista() {
        return this.dao.findAll();
    }
    
}
