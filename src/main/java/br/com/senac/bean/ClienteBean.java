/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.bean;

import br.com.senac.banco.ClienteDAO;
import br.com.senac.livraria.entity.Cliente;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author wilso
 */
@Named(value = "clienteBean")
@ViewScoped
public class ClienteBean extends Bean {

    private Cliente cliente;
    private ClienteDAO dao;

    public ClienteBean() {
    }
    
    
    @PostConstruct
    public void init(){
        this.cliente = new Cliente();
        this.dao = new ClienteDAO();
        
        
    }
    
    
    public String getCodigo(){
        return this.cliente.getId() == 0 ? "" : String.valueOf(this.cliente.getId());
    }
    
    
    public void novo(){
        this.cliente = new Cliente();
    }
    
    
     public void salvar() {
        
        try {
            
            if (this.cliente.getId() == 0) {
                dao.save(cliente);
                addMessageInfo("Salvo com sucesso!");
            } else {
                dao.update(cliente);
                addMessageInfo("Alterado com sucesso!");
            }
            
        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }
        
    }
    
    
         public void excluir(Cliente cliente) {
        try {
            
            dao.delete(cliente.getId());
            addMessageInfo("Removido com sucesso!");
            
        } catch (Exception ex) {
            addMessagemErro(ex.getMessage());
        }
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

   
    public List<Cliente> getLista() {
        return this.dao.findAll();
    }
    
    
    
    
    
    
    
    

}
