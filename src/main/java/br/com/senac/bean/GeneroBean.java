/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.bean;

import br.com.senac.banco.GeneroDAO;
import br.com.senac.livraria.entity.Genero;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Administrador
 */
@Named(value = "generoBean")
@RequestScoped
public class GeneroBean {

    private Genero genero = new Genero();

    private GeneroDAO dao = new GeneroDAO();

    public GeneroBean() {

    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public void salvar() {

        try {

            if (this.genero.getId() == 0) {
                dao.save(genero);
            } else {
                dao.update(genero);
            }

            FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Salvo", "Salvo com sucesso");
            context.addMessage("dasdfgv", message);

        } catch (Exception ex) {
        }

    }
    
    public void novo(){
        this.genero = new Genero();
    }
    
    
    public List<Genero> getLista(){
        return dao.findAll();
    }
    

}
