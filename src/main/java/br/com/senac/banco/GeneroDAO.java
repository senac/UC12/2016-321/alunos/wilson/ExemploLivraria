/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.banco;

import br.com.senac.livraria.entity.Genero;

/**
 *
 * @author Administrador
 */
public class GeneroDAO extends DAO<Genero>{
    
    public GeneroDAO() {
        super(Genero.class);
    }
    
}
